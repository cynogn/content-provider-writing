package com.gautam.customcontentprovider;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

/**
 * This Class allows to Add,update,delete and view data in the content provider
 * 
 * @author GAUTAM B
 * 
 * @Versaion 1
 * 
 */
public class HomescreenActivity extends Activity implements OnClickListener {

	/**
	 * Emplyee Name Edit Text
	 */
	private EditText mNameEditText;//

	/**
	 * 
	 */
	private EditText mRollNoText;// Emplyee Number Edit Text
	private EditText mValue; // unique ID editText
	private Button mAdd;// Add button
	private Button mUpdate;// Update button
	private Button mDelete;// Delete button
	private String[] projection;// all rows of the Employee databse
	private String[] uiBindFrom;// customadatper row values
	private ListView mListView;// listView
	private CursorAdapter adapter;// Simplecursoradapter
	private int[] uiBindTo;// the ID odf the custom Row
	private Cursor tutorials;// Cursor

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.homescreen);
		mNameEditText = (EditText) findViewById(R.id.name);
		mValue = (EditText) findViewById(R.id.editText1);
		mRollNoText = (EditText) findViewById(R.id.rollno);
		mAdd = (Button) findViewById(R.id.button1);
		mUpdate = (Button) findViewById(R.id.button2);
		mDelete = (Button) findViewById(R.id.button3);
		uiBindFrom = new String[] { "_id", "EmployeeName", "RollNumber" };
		uiBindTo = new int[] { R.id.textView1, R.id.textView2, R.id.textView3 };
		mListView = (ListView) findViewById(R.id.listView1);
		mAdd.setOnClickListener(this);
		mUpdate.setOnClickListener(this);
		mDelete.setOnClickListener(this);
		projection = new String[] { EmployeeDatabase.ID,
				EmployeeDatabase.COL_TITLE, EmployeeDatabase.COL_URL };
		/*
		 * ContentValues values = new ContentValues();
		 * values.put(EmployeeDatabase.COL_TITLE, "1");
		 * values.put(EmployeeDatabase.COL_URL, "1");
		 * getContentResolver().insert(TutListProvider.CONTENT_URI, values);
		 */
		setAdapterforlsit();

		tutorials.moveToLast();
		mValue.setText(tutorials.getInt(0) + "");

		/*
		 * Wating the text change of the Inoque id field
		 */
		mValue.addTextChangedListener(new TextWatcher() {

			/*
			 * load the values and display in the screen
			 */
			@Override
			public void afterTextChanged(Editable s) {
				loadValuesFromCursor(s.toString());

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

			}
		});

		/*
		 * binding the adapter and listView
		 */
		mListView.setAdapter(adapter);
		mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {

				TextView tv = (TextView) (arg1).findViewById(R.id.textView1);
				String value = tv.getText().toString();

				/*
				 * Toast.makeText(getApplication(), value + "is the value",
				 * Toast.LENGTH_SHORT).show();
				 */
				loadValuesFromCursor(value);

				mValue.setText(tv.getText().toString());

			}
		});
		mListView.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				TextView tv = (TextView) findViewById(R.id.textView1);
				String value = tv.getText().toString();
				alertDialogBox(Integer.parseInt(value));
				return false;

			}
		});
	}

	/*
	 * set the
	 */
	private void setAdapterforlsit() {
		tutorials = this.managedQuery(TutListProvider.CONTENT_URI, projection,
				null, null, null);
		adapter = new SimpleCursorAdapter(this.getApplicationContext(),
				R.layout.list_item, tutorials, uiBindFrom, uiBindTo);
		adapter.notifyDataSetChanged();
		mListView.setAdapter(adapter);

	}

	/*
	 * binds the cursor and the lsit view
	 */
	void clearValues() {
		Cursor tutorials = this.managedQuery(TutListProvider.CONTENT_URI,
				projection, null, null, null);
		tutorials.moveToLast();
		// mValue.setText(tutorials.getInt(0) + "");
		mNameEditText.setText("");
		mRollNoText.setText("");
		setAdapterforlsit();

	}

	/**
	 * loads the values from the cursor and display in the name and roll number
	 * text box
	 */
	void loadValuesFromCursor(String count) {
		try {
			int i = Integer.parseInt(count);
			Cursor tutorials = this.managedQuery(TutListProvider.CONTENT_URI,
					projection, "_id=" + i, null, null);
			tutorials.moveToFirst();

			mNameEditText.setText(tutorials.getString(1));
			mRollNoText.setText(tutorials.getString(2));
			Toast.makeText(getApplicationContext(), tutorials.getInt(0),
					Toast.LENGTH_SHORT).show();

		} catch (Exception e) {
			Toast.makeText(getApplicationContext(), "ERROR", Toast.LENGTH_SHORT)
					.show();
		}

	}

	/*
	 * on click handles insert or update or delete
	 */
	@Override
	public void onClick(View v) {
		/*
		 * Inserting the values
		 */
		if (v.getId() == R.id.button1) {
			ContentValues values = new ContentValues();
			values.put(EmployeeDatabase.COL_TITLE, mNameEditText.getText()
					.toString());
			values.put(EmployeeDatabase.COL_URL, mRollNoText.getText()
					.toString());
			getContentResolver().insert(TutListProvider.CONTENT_URI, values);

			Toast.makeText(HomescreenActivity.this, "Added!",
					Toast.LENGTH_SHORT).show();
			// functional call to clear values
			clearValues();
		}

		else
		/*
		 * Updating the values
		 */
		if (mUpdate.getId() == v.getId()) {

			ContentValues values = new ContentValues();
			values.put(EmployeeDatabase.COL_TITLE, mNameEditText.getText()
					.toString());
			values.put(EmployeeDatabase.COL_URL, mRollNoText.getText()
					.toString());
			getContentResolver().update(TutListProvider.CONTENT_URI, values,
					"_id = " + (Integer.parseInt(mValue.getText().toString())),
					null);

			Toast.makeText(HomescreenActivity.this, "Updated!",
					Toast.LENGTH_SHORT).show();
			// functional call to clear values
			clearValues();
		} else
		/*
		 * Deleting the values
		 */
		if (mDelete.getId() == v.getId()) {
			alertDialogBox(Integer.parseInt(mValue.getText().toString()));
		}
	}

	/*
	 * confirm dialog box creation
	 */
	public void alertDialogBox(final int delValue) {

		AlertDialog.Builder builder = new AlertDialog.Builder(
				HomescreenActivity.this);
		builder.setTitle("Are you sure want to delete?");
		/*
		 * positive button in the dialog
		 */
		builder.setPositiveButton("Yes!Delete",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int item) {

						int delete = -1;
						delete = getContentResolver().delete(
								TutListProvider.CONTENT_URI,
								"_id = " + delValue, null);
						/*
						 * checking whther its deleted or not
						 */
						if (delete != 0)
							Toast.makeText(HomescreenActivity.this, "Delete!",
									Toast.LENGTH_SHORT).show();
						else
							Toast.makeText(HomescreenActivity.this,
									"Row not Found!", Toast.LENGTH_SHORT)
									.show();
						// functional call to clear values
						clearValues();
					}
				});
		/*
		 * negative button in the dialog
		 */
		builder.setNegativeButton("Revert",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
					}
				});
		/*
		 * shows the dailog box on click of delete
		 */
		AlertDialog alert = builder.create();
		alert.show();
	}

}
