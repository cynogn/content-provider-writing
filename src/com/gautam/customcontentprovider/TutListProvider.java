package com.gautam.customcontentprovider;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

/**
 * Custom Content Provider
 * 
 * @author GAUTAM BALASUBRAMANIAN
 * 
 * @version 1
 * 
 */
public class TutListProvider extends ContentProvider {
	private EmployeeDatabase mDB;
	private static final String AUTHORITY = "com.gautam.customcontentprovider.TutListProvider";
	public static final int TUTORIALS = 100;
	public static final int TUTORIAL_ID = 110;

	private static final String TUTORIALS_BASE_PATH = "tutorials";
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
			+ "/" + TUTORIALS_BASE_PATH);

	public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
			+ "/mt-tutorial";
	public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
			+ "/mt-tutorial";

	private static final UriMatcher sURIMatcher = new UriMatcher(
			UriMatcher.NO_MATCH);
	static {
		sURIMatcher.addURI(AUTHORITY, TUTORIALS_BASE_PATH, TUTORIALS);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.content.ContentProvider#onCreate()
	 */
	@Override
	public boolean onCreate() {
		mDB = new EmployeeDatabase(getContext());

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.content.ContentProvider#delete(android.net.Uri,
	 * java.lang.String, java.lang.String[])
	 */
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		int uriType = sURIMatcher.match(uri);
		SQLiteDatabase sqlDB = mDB.getWritableDatabase();
		int rowsAffected = 0;
		switch (uriType) {
		case TUTORIALS:
			rowsAffected = sqlDB.delete(EmployeeDatabase.TABLE_TUTORIALS,
					selection, selectionArgs);
			break;

		default:
			throw new IllegalArgumentException("Unknown or Invalid URI " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsAffected;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.content.ContentProvider#getType(android.net.Uri)
	 */
	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.content.ContentProvider#insert(android.net.Uri,
	 * android.content.ContentValues)
	 */
	@Override
	public Uri insert(Uri uri, ContentValues values) {
		int uriType = sURIMatcher.match(uri);
		String tableName;
		switch (uriType) {

		case TUTORIALS:
			tableName = EmployeeDatabase.TABLE_TUTORIALS;
			SQLiteDatabase sqlDB = mDB.getWritableDatabase();
			sqlDB.insert(tableName, null, values);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI");
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.content.ContentProvider#query(android.net.Uri,
	 * java.lang.String[], java.lang.String, java.lang.String[],
	 * java.lang.String)
	 */
	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
		queryBuilder.setTables(EmployeeDatabase.TABLE_TUTORIALS);
		Cursor cursor = queryBuilder.query(mDB.getReadableDatabase(),
				projection, selection, selectionArgs, null, null, sortOrder);
		cursor.setNotificationUri(getContext().getContentResolver(), uri);
		return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		String tableName;
		int uriType = sURIMatcher.match(uri);
		switch (uriType) {
		case TUTORIALS:
			/*
			 * updating the row value
			 */
			tableName = EmployeeDatabase.TABLE_TUTORIALS;
			SQLiteDatabase sqlDB = mDB.getWritableDatabase();
			sqlDB.update(tableName, values, selection, selectionArgs);
			break;
		}
		return 0;
	}
}