package com.gautam.customcontentprovider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Employee class extending SQLiteOpenHelper
 * 
 * @author GAUTAM BALASUBRAMANIAN
 * 
 * @version 1
 * 
 */

public class EmployeeDatabase extends SQLiteOpenHelper {
	private static final String DEBUG_TAG = "EmployeeDatabase";
	private static final int DB_VERSION = 2;
	private static final String DB_NAME = "employee";
	public static final String TABLE_TUTORIALS = "emplyeeDetails";
	public static final String ID = "_id";
	public static final String COL_TITLE = "EmployeeName";
	public static final String COL_URL = "RollNumber";

	private static final String DB_SCHEMA = "create table " + TABLE_TUTORIALS
			+ " (" + ID + " integer primary key autoincrement, " + COL_TITLE
			+ " text , " + COL_URL + " text );";

	/**
	 * EmployeeDatabse constructor
	 */
	public EmployeeDatabase(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	/**
	 * Creates the tables
	 */
	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.v(DEBUG_TAG, "Created the table");
		db.execSQL(DB_SCHEMA);
	}

	/**
	 * Updates the existing table
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TUTORIALS);
		onCreate(db);
	}
}
