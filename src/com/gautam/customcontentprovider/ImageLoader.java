package com.gautam.customcontentprovider;

import java.io.InputStream;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import android.graphics.drawable.Drawable;

class ImageLoader {
	private static HashMap<String, Drawable> imagesMap = new HashMap<String, Drawable>();

	public void imgLoader(final String url) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				imageLoader(url);
				System.out.println(imagesMap);
			}
		}).start();

	}

	private Drawable imageLoader(String url) {
		Collection<Drawable> c = imagesMap.values();
		Iterator<Drawable> itr = c.iterator();
		if (imagesMap.containsKey(url)) {
			return (Drawable) itr.next();
		} else {
			Drawable d = LoadImageFromWeb(url);
			imagesMap.put(url, d);
			return d;
		}
	}

	private Drawable LoadImageFromWeb(String url) {
		try {
			InputStream is = (InputStream) new URL(url).getContent();
			Drawable d = Drawable.createFromStream(is, "src name");
			return d;
		} catch (Exception e) {
			System.out.println("Exc=" + e);
			return null;
		}
	}
}
